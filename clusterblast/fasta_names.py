#!/usr/bin/python2

def fastanames(fasta):
  names = []
  file = open(fasta,"r")
  text = file.read()
  text = text.strip()
  #Replaces all spaces with "_" to avoid problems
  text = text.replace(' ','_')
  text = text.split()
  for a in text:
    if ">" in a[0]:
      f = str()
      d = a[1:]
      names.append(d)
  return names
    
def fastadict(fasta):
  file = open(fasta,"r")
  text = file.read()
  text = text.strip()
  #Replaces all spaces with "_" to avoid problems
  text = text.replace(' ','_')
  text = text.split()
  dictseq = {}
  for a in text:
    if ">" in a[0]:
      f = str()
      d = a[1:]
    else:
      e = a
      f += e
      dictseq[d] = f
  return dictseq

def fastaseqs(names,fastadict):
  seqs = []
  for i in names:
    seq = fastadict[i]
    seqs.append(seq)
  return seqs

def writefasta(names,seqs,file):
  e = 0
  f = len(names) - 1
  out_file = open(file,"w")
  while e <= f:
    out_file.write(">")
    out_file.write(names[e])
    out_file.write("\n")
    out_file.write(seqs[e])
    out_file.write("\n")
    e += 1
  out_file.close()

import sys
infile = sys.argv[1]
outfile = sys.argv[2]
names = fastanames(infile)
seqs = []
for i in names:
  seqs.append("")
writefasta(names,seqs,outfile)
