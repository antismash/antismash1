// ReactionClass.inl contains all the important reaction functions 
// for both pre- and post- assembly, including cyclization, 
// O, N, and C methylations, hydroxylation, halogenation,
// nucleophile cyclization, and glycosylation   

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

#include "constants.h"
#include "UnitClass.h"
#include "QueueClass.h"
#include "massAndFormula.h"
#include "libGen.h"

void ReactionClass::addSmileToLibrary(const string &SMILE, QueueClass< UnitClass > &products)
{
  UnitClass *peptide;
  float mass;
  string formula;

  peptide = new UnitClass;
  massAndFormula( SMILE, mass, formula );
  peptide = new UnitClass;
  peptide->setUnit( "0", SMILE);
  peptide->setMass( mass );
  peptide->setFormula( formula );
  products.insertValue( *peptide );
  delete peptide;
}


void ReactionClass::tripleEster( const string &inPeptide, QueueClass< UnitClass > &products)
{
  // Ester cyclization
  float mass;
  string formula;
  int lastInsert = 0;
  string esterCycNum = "0";
  int strainLength = 0;
  string tempPeptide = inPeptide;
  bool ester = false;
  string dimerizedEster;
  string trimerizedEster;
  string disulfideDimer;
  string linearEsterDimer;
  string disulfidedMol;
  bool addedSbond = false;

  // search for Oxygen and Sulfur
  for ( int k = lastInsert; k < tempPeptide.length() - 1; k++ ) // count
  {  
    if( (tempPeptide[k] == 'O' || tempPeptide[k] == 'S' )  
          && tempPeptide[k+1] == ')' && tempPeptide[k-1] != '='
          && tempPeptide[k-2] != 'c'
          && tempPeptide.length() - k > strainLength)  
    {
      ester = true;
    }
  }
  // Ester trimerization (e.g. thiocoraline)
  if(ester)
  {
    tempPeptide = inPeptide;
    dimerizedEster = esterDimerize(tempPeptide, tempPeptide, linearEsterDimer);
    trimerizedEster = esterDimerize(linearEsterDimer, tempPeptide, linearEsterDimer);
    massAndFormula( trimerizedEster, mass, formula );
    UnitClass *peptide = new UnitClass;
    peptide = new UnitClass;
    peptide->setUnit( "0", trimerizedEster );
    peptide->setMass( mass );
    peptide->setFormula( formula );
    products.insertValue( *peptide );
    delete peptide;

    linearEsterDimer.insert(linearEsterDimer.length(), "O");
    massAndFormula( linearEsterDimer, mass, formula );
    peptide = new UnitClass;
    peptide->setUnit( "0", linearEsterDimer);
    peptide->setMass( mass );
    peptide->setFormula( formula );
    products.insertValue( *peptide );
    delete peptide;
  }
}


void ReactionClass::amideCyc( const string &inPeptide, QueueClass< UnitClass > &products)
{
  float mass;
  string formula;
  int insertFirstZero;
  string esterCycNum = "0";
  int strainLength = 25;
  int cycLim = 15;
  string tempPeptide = inPeptide;
  string disulfidedMol;
  UnitClass *peptide;
  int findN;
  int findN1;

  findN = tempPeptide.find("N");
  findN1 = tempPeptide.find("N1");

  if(tempPeptide.length() > strainLength && (findN >= 0 && findN < cycLim) 
     || (findN1 >= 0 && findN1 < cycLim ))
  {  
     if(findN == findN1)
     {
       tempPeptide.insert(findN1+1, "0");
     }
     else
     {
       tempPeptide.insert(findN+1, "0");
     }
     
     tempPeptide.insert(tempPeptide.length()-4, "0");

    // TE cyclic carboxyl peptide product
      massAndFormula( tempPeptide, mass, formula );    // get mass and formula
      peptide = new UnitClass;
      peptide->setUnit( "0", tempPeptide);            // set name and SMILES
      peptide->setMass( mass );                       // set mass
      peptide->setFormula( formula );                 // set formula
      products.insertValue( *peptide );
      //cout << endl;
      //cout << "cyclized amide: " << tempPeptide << endl;
      delete peptide;
  }
}

void ReactionClass::esterCycDim( const string &inPeptide, QueueClass< UnitClass > &products, const string dimerize)
{
  // Ester cyclization
  float mass;
  string formula;
  int lastInsert = 0;
  string esterCycNum = "0";
  int strainLength = 25;
  string tempPeptide = inPeptide;
  bool ester = false;
  string dimerizedEster;
  string disulfideDimer;
  string linearEsterDimer;
  string disulfidedMol;
  int firstEster;
  int firstEsterLim = 50;

  // search for Oxygen and Sulfur
  for ( int k = lastInsert; k < tempPeptide.length() - 1; k++ ) // count
  {  
    if((tempPeptide[k] == 'O' || tempPeptide[k] == 'S' || (tempPeptide[k] == 'N' 
        && tempPeptide[k-1] == 'C' && tempPeptide[k-2] == 'C'))  
        && tempPeptide[k+1] == ')' && tempPeptide[k-1] != '='
        && tempPeptide[k-2] != 'c' && tempPeptide[k-2] != 'O'
        && tempPeptide.length() - k > strainLength)  
    {
      tempPeptide.insert(k+1, esterCycNum);
      tempPeptide.insert(tempPeptide.length()-4, esterCycNum);
      massAndFormula( tempPeptide, mass, formula );
      UnitClass *peptide = new UnitClass;
      peptide = new UnitClass;
      peptide->setUnit( "0", tempPeptide);
      peptide->setMass( mass );
      peptide->setFormula( formula );
      products.insertValue( *peptide );
      delete peptide;
      lastInsert = k+2;
      tempPeptide = inPeptide;
      ester = true;
      firstEster = k;
    }
  }
  cout << "products list after oxygen/sulfur search: " << endl;
  for(int i = 0; i < products.getNumElems(); i++)
  {
    cout << products.getElemAtIndex(i).getSmile() << endl;
  }
  
  // Ester dimerization (e.g. thiocoraline)
  if(dimerize[0] == 'D' && dimerize[1] == 'I' && dimerize[2] == 'M')
  {
    //cout << "DIMERIZING!!!!!!!!!!!!!!!" << endl;
    if(ester && firstEster < firstEsterLim)
    {
      tempPeptide = inPeptide;
      dimerizedEster = esterDimerize(tempPeptide, tempPeptide, linearEsterDimer);
      massAndFormula( dimerizedEster, mass, formula );
      UnitClass *peptide = new UnitClass;
      peptide = new UnitClass;
      peptide->setUnit( "0", dimerizedEster );
      peptide->setMass( mass );
      peptide->setFormula( formula );
      products.insertValue( *peptide );
      delete peptide;
   
      cout << "products after dimerization: " << endl;
      for(int i = 0; i < products.getNumElems(); i++)
      {
        cout << products.getElemAtIndex(i).getSmile() << endl;
      } 
      disulfideBond(dimerizedEster, products, 1);
      cout << "products after disulfide bond: " << endl;
   
      for(int i = 0; i < products.getNumElems(); i++)
      {
        cout << products.getElemAtIndex(i).getSmile() << endl;
      }
   
      linearEsterDimer.insert(linearEsterDimer.length(), "O");
      massAndFormula( linearEsterDimer, mass, formula );
      peptide = new UnitClass;
      peptide->setUnit( "0", linearEsterDimer);
      peptide->setMass( mass );
      peptide->setFormula( formula );
      products.insertValue( *peptide );
      delete peptide;

      cout << "products after lienarEsterDimer: " << endl;
      for(int i =0; i < products.getNumElems(); i++)
      {
        cout << products.getElemAtIndex(i).getSmile() << endl;
      }
    }
  }  
}
int ReactionClass::aminoAcidCounter( const string &inSMILE)
{
  int aminoAcidCount = 0;
  for(int i = 0; i < inSMILE.length(); i++)
  {
    if((inSMILE[i] == '@' && inSMILE[i-1] == '@')
      ||(inSMILE[i] == 'C' && inSMILE[i-1] == 'C'
         && inSMILE[i-2] == 'N') 
      ||(inSMILE[i] == 'C' && inSMILE[i-1] == 'N' 
         && inSMILE[i-2] == 'C' && inSMILE[i-3] == ')'))
    {
      aminoAcidCount++;
    }
  }
  return aminoAcidCount;
}

string ReactionClass::reverseSMILES( const string &forSMILES)
{
  string revSMILES = "";
  int bracket = 0;
  int iStore;  
 
  // go through forward SMILES backwards 
  for(int i = forSMILES.length(); i >= 0; i--)
  {
    iStore = i;
    if(forSMILES[i] == ')' // if there is a close parentheses
       || forSMILES[i] == ']')
    { 
      bracket++;
    }

    if(forSMILES[i] == '(' // if there is a close parentheses
       || forSMILES[i] == '[')
    { 
      bracket--;
    }           
    while(i != 0 && (bracket != 0 || (forSMILES[i-1] == ']' && forSMILES[i] == '(')
          || (forSMILES[i] == '(' && (forSMILES[i+1] == '='
          || forSMILES[i+1] == 'C' || forSMILES[i+1] == 'O'
          || forSMILES[i+1] == 'S' || forSMILES[i+1] == '[' 
          || forSMILES[i+1] == 'N'))
          || (static_cast< unsigned int >( forSMILES[i]) >= 48 && 
		static_cast< unsigned int >( forSMILES[i]) <= 57 )
            || (static_cast< unsigned int >( forSMILES[i]) >=97 &&
                static_cast< unsigned int >( forSMILES[i]) <= 122)))
    { 
      i--;
      if(forSMILES[i] == ')' // if there is a close parentheses
         || forSMILES[i] == ']')
      { 
        bracket++;
      }

      if(forSMILES[i] == '(' // if there is a close parentheses
         || forSMILES[i] == '[')
      { 
        bracket--;
      }       
    }

    revSMILES.insert(revSMILES.length(), forSMILES.substr(i, iStore-i+1));  
  }
  return revSMILES;
}

string ReactionClass::esterDimerize( const string &forSMILES, const string &smileToRev, string &linearEsterDimer)   
{
  string SMILEScopy; 
  string revSMILES = ""; 
  string formula;
  int firstUnit = 2;
  int linkLimit = 7;
  int toGoBack;
  int toGoBackBuffer = 3;
  int back = 0;
  bool dimerized = 0;
  SMILEScopy = forSMILES;
  revSMILES = reverseSMILES(smileToRev);
  //cout << "Reversed SMILES: "<< endl;
  //cout << revSMILES << endl;
  
  toGoBack = SMILEScopy.length() - toGoBackBuffer;  
  cout << "reversedSMILES:" << revSMILES << endl;

  if(SMILEScopy.length() > 0)
  {
    for(int i = 0; i < SMILEScopy.length(); i++)
    {
      if((SMILEScopy[i] == 'S' ||  
          SMILEScopy[i] == 'O') && SMILEScopy[i+1] == ')'
          && SMILEScopy[i-1] != '=' 
          && SMILEScopy[i-2] != 'c'
          && dimerized == 0)
      {
        dimerized = 1;
        SMILEScopy.insert(i+1, "(");
        SMILEScopy.insert(i+2, revSMILES);
        SMILEScopy.insert(i+2+revSMILES.length(), ")");
        linearEsterDimer = SMILEScopy;
        i = i + 2 + revSMILES.length();
        while((SMILEScopy[i-2] == '='  
              || SMILEScopy[i] != ')' 
              || (SMILEScopy[i-1] != 'O' 
		  && SMILEScopy[i-1] != 'S') ))
	{
          i--;
          back++;
          cout << "running first while" << endl;
	}
        cout << "scan: " << SMILEScopy[i-2] 
        << SMILEScopy[i-1] << SMILEScopy[i]
        << SMILEScopy[i+1] << SMILEScopy[i+2] << endl;
 
        SMILEScopy.insert(i, "0"); // INSERT CYCLIZATION
        i = SMILEScopy.length();
        while(SMILEScopy[i] != 'C' && SMILEScopy[i+2] != '=')
	{
          i--;
	}
        SMILEScopy.insert(i+1, "0");
	SMILEScopy.erase(SMILEScopy.length(), 1);
      }
    }
  }

  //cout << "Dimerized SMILES: " << endl;
  //cout << SMILEScopy << endl;

  return SMILEScopy;
}

void ReactionClass::disulfideBond( const string &inSMILES,
                                   QueueClass< UnitClass > &products, const int &mode)
{
  int aminoAcidCount = 0;
  int moleculeLength;
  int disulfide;
  int centerMolecule;
  int prevSulfur;
  int nextSulfur;
  int sulfurWidth = 10;
  int rightSulfur;
  int leftSulfur;
  int rightDiff;
  int leftDiff;
  int iStore = 0;
  int jStore = 0;
  string tempPeptide = inSMILES;
  string disulfideNum = "7";
  float mass;
  string formula;
  UnitClass *peptide;

  if(mode == 0)
  {
    //cout << "in mode 0" << endl;
    for(int i = iStore; i < inSMILES.length(); i++)
    {
      //cout << "inSMILES: " << inSMILES << endl;
      //cout << "inSMILES.length(): " << inSMILES.length() << endl;
      prevSulfur = inSMILES.find("CS)", iStore);
      //cout << "prefSulfur: " << prevSulfur << endl; 
      nextSulfur = inSMILES.find("CS)", prevSulfur+inSMILES.length()/2 - sulfurWidth);
      //cout << "nextSulfur: " << nextSulfur << endl;
      if(prevSulfur > 0 && nextSulfur > 0)
      {
        if(nextSulfur - prevSulfur < inSMILES.length()/2 + sulfurWidth)
        {
          tempPeptide.insert(prevSulfur+2, disulfideNum);
          tempPeptide.insert(nextSulfur+3, disulfideNum);
          addSmileToLibrary(tempPeptide, products);
          tempPeptide = inSMILES;
          iStore = prevSulfur + sulfurWidth;
        }
      }
      if(prevSulfur > inSMILES.length()/2 || nextSulfur < 0)
      {
        i = inSMILES.length();
      }      
    }   
  }
  else if(mode == 1) // dimer with flipped addition (e.g. thiocoraline, echinomycin)
  {
    //cout << "in mode 1" << endl;
    centerMolecule = inSMILES.find("CS0", 0);
    if(centerMolecule < 0)
    {
      centerMolecule = inSMILES.find("CO0", 0);
    }

    if(centerMolecule > 0)
    {
      jStore = centerMolecule;
      //cout << "centerMolecule: " << centerMolecule << endl;
      //cout << "inSMILES.length()/2: " << inSMILES.length() << endl;
      for(int i = centerMolecule; i < inSMILES.length(); i++)
      {
        if(inSMILES[i-2] == 'C' && inSMILES[i-1] == 'S' 
           && inSMILES[i] == ')')
        {  
          rightSulfur = i-1;
          //cout << "rightSulfur: " << rightSulfur << endl;
          for(int j = jStore; j > 0; j--)
          {
            if(inSMILES[j] == 'C' && inSMILES[j+1] == 'S'
               && inSMILES[j+2] == ')')
            {
              leftSulfur = j+1;
              //cout << "leftSulfur: " << leftSulfur << endl;
              jStore = j - 1;
              j = 0;
              if(rightSulfur && leftSulfur > 1)
              { 
                rightDiff = rightSulfur - centerMolecule;
                leftDiff = centerMolecule - leftSulfur;
                if(rightDiff - leftDiff < sulfurWidth 
                   && rightDiff - leftDiff > -1 * sulfurWidth)
                {
                  tempPeptide.insert(rightSulfur+1, disulfideNum);
                  tempPeptide.insert(leftSulfur+1, disulfideNum);
                  addSmileToLibrary(tempPeptide, products);
                  tempPeptide = inSMILES;
                }
              } 
            }
          } 
        }
      }
    } // finish sulfurSearching for loop
  } 
}

//Function for both cyclization and dimerization
void ReactionClass::cyclization( const string inPeptide,
                                 const string terminType,
                                 QueueClass< UnitClass > &products,
                                 const string dimerize )
{
  string linearPep;
  string cyclicPep;
  string formula;
  string trimer;
  string dimer;
  string triSer;
  string triThr;
  string triCys;
  string tempPeptide;
  float mass;
  UnitClass *peptide;
  int numAminoAcids;
  int triMin = 3;
  int dimMin = 2;
  int dimMax = 5;
  int disulfideMin = 6;
  int length;
  //numAminoAcids = aminoAcidCounter(inPeptide);

  // macrocyclic amido-peptide product
  if ( terminType == "TE")
  {
      linearPep = inPeptide;
      linearPep.append("O");
    // TE non-cyclic carboxyl peptide product (default)
      massAndFormula( linearPep, mass, formula );    // get mass and formula
      peptide = new UnitClass;
      peptide->setUnit( "0", linearPep);            // set name and SMILES
      peptide->setMass( mass );                       // set mass
      peptide->setFormula( formula );                 // set formula
      products.insertValue( *peptide );
      cout << endl;
      //cout << "linearPep: " << linearPep << endl;
      delete peptide;

    // TE cyclic peptide amide products
    amideCyc( inPeptide, products);
     
    cout << "amideCyc products" << endl; 
    for(int i = 0; i < products.getNumElems(); i++)
    {
      cout << products.getElemAtIndex(i).getSmile() << endl;
    }

    // TE cyclic ester dimer
    esterCycDim( inPeptide, products, dimerize);
  }
  cout << "Products list at end of cyclization:" << endl;
  for(int i = 0; i < products.getNumElems(); i++)
  {
    cout << products.getElemAtIndex(i).getSmile() << endl;
  }
}

string ReactionClass::preAssemblyReactions( const string smileName,
                                            const string inString)
{
  string tempSmileName = smileName;
  string outSmile;
  size_t location;

  // epimerization at chiral carbon
  if( inString == "EPI" )
  {
    outSmile = epimerization( smileName );
  }

  // General methylation. The prefered methylation site is alpha-amino group.
  // When alpha-amino group is not available, methylate side chain amino or
  // hydroxyl group, if available.
  if ( inString == "ME" )
  {
    location = smileName.find( "N[" );
    if ( location != string::npos )
    {
      outSmile = tempSmileName.insert( int( location )+1, "(C)" );
    }
    else
    {
      size_t location1 = smileName.find( "N", location+2 );
      if ( location1 != string::npos )
      {
        outSmile = N_Methylation( smileName );
      }
      else
      {
        location = smileName.find( "O", location+2 );
        if ( location != string::npos )
        {
          outSmile = O_Methylation( smileName );
        }
      }
    }
  }

  // N-methylation at free amino group
  if ( inString == "NME" )
  {
    outSmile = N_Methylation( smileName );
  }

  // O-methylation at free hydroxyl group
  if ( inString == "OME" )
  {
    outSmile = O_Methylation( smileName );
  }

  // hydroxylation of benzyl carbon
  if ( inString == "HYD" )
  {
    outSmile = hydroxylation( smileName );
  }
   cout << outSmile << endl;
   return ( outSmile );
}


// epimerization at chiral carbon
string ReactionClass::epimerization( const string smileName )
{
  string tempSmileName = smileName;
  size_t location;
  int smileLength = smileName.length();
  int atCount = 0;

  for( int i = 0; i < smileLength - 1; i++ )
  {
    if( smileName[i] == '@' && smileName[i+1] == '@' )
    {
      tempSmileName.erase( i, 1 );
      i = smileLength;
    }
    if( smileName[i] == '@' && smileName[i+1] != '@' )
    {
      tempSmileName.insert( i, "@" );
      i = smileLength;
    }
  }
  return ( tempSmileName );
}

string ReactionClass::N_Methylation( const string smileName )
{
  string tempSmileName = smileName;
  size_t location;

  location = smileName.find( "C(=O)N)" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+6, "C" );
  }
  location = smileName.find( "C(=O)NC" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+6 , "(C)" );
  }
  location = smileName.find( "(N)" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+2, "C" );
  }
  location = smileName.find( "CN)" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+2, "C" );
  }
  location = smileName.find( "(NC)" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+2, "(C)" );
  }
  location = smileName.find( "CNC" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+2, "(C)" );
    location = tempSmileName.find( "CN(C)C(N" );  // special case for ARG
    if ( location != string::npos )
    {
      tempSmileName.erase( int( location)+2, 3 );
    }
  }
  return ( tempSmileName );
}

string ReactionClass::O_Methylation( const string smileName )
{
  string tempSmileName = smileName;
  size_t location;

  location = smileName.find( "CO)" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+2, "C" );
  }
  location = smileName.find( "(O)" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+2, "C" );
  }
  return ( tempSmileName );
}

string ReactionClass::S_Methylation( const string smileName)
{
  string tempSmileName = smileName;
  size_t location;

  location = smileName.find( "CS)" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+2, "C" );
  }
  location = smileName.find( "(S)" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+2, "C" );
  }
  return ( tempSmileName );
}

// hydroxylation happens at benzyl carbon before at aromatic ring
string ReactionClass::hydroxylation( const string smileName )
{
  string tempSmileName = smileName;
  size_t location;

  // reaction at Phe and Tyr
  location = smileName.find( "C(O)c1" );
  if ( location != string::npos )
  {
    location = smileName.find( "c1ccc(O)cc1" );
    if ( location != string::npos )
    {
      tempSmileName.insert( int( location )+4, "(O)" );
    }
    location = smileName.find( "c1ccccc1" );
    if ( location != string::npos )
    {
      tempSmileName.insert( int( location )+5, "(O)" );
    }
  }
  location = smileName.find( "Cc1c" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+1, "(O)" );
  }

  // reaction at Trp and His
  location = smileName.find( "(C(O)C1=CNc2c1cccc2)" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+19, "O" );
  }
  location = smileName.find( "(CC1=CN" );
  if ( location != string::npos )
  {
    tempSmileName.insert( int( location )+2, "(O)" );
  }
  return ( tempSmileName );
}

void ReactionClass::postAssemblyReactions (
                           QueueClass< string > &postModifications,
                           QueueClass< UnitClass > &postProducts,
                           QueueClass< UnitClass > &emptyLibrary
                                          )
{
  int countMods;
  int modNum = 0;
  int modArraySize;
  int numPeptides = postProducts.getNumElems();
  int numModifications;
  int ringNum = 3;
  int tempLength;
  float mass;
  bool postModLoop = true;
  string postMod;
  string formula;
  string tempPeptide;
  string revTempPeptide;
  bool forw = 1;
  bool rev = 0;

  QueueClass< string > modArray;

  QueueClass< UnitClass > *peptideLibrary;
  peptideLibrary = new QueueClass< UnitClass >;

  QueueClass< UnitClass > *postCopy = new QueueClass< UnitClass >;
  postCopy =  &postProducts;

  QueueClass< UnitClass > postProductsCopy;
  postProductsCopy = postProducts;
  UnitClass peptide;

  numModifications = postModifications.getNumElems();
  
  //cout << "modded library elems: " << postProducts.getNumElems() << endl;
  cout << "# of postMods: " << numModifications << endl;

  if( postModifications.getNumElems() )
  {
    // iterate through modification
    for( int i = 0; i < numModifications; i++ )
    {
      //cout << "postMod outer loop: " << postModifications.getElemAtIndex(i) << endl;
      //cout << "numPeptides: " << numPeptides << endl;
      //cout << "peptide Library size: " << peptideLibrary->getNumElems() << endl;

      ringNum = 3;
      // iterate through peptides
      for( int j = 0; j < numPeptides; j++ )
      {
        
        //cout << "i: " << i << endl;
        //cout << "postMod inner loop: " << postModifications.getElemAtIndex(i) << endl;
        tempPeptide = postCopy->getElemAtIndex(j).getSmile();
        revTempPeptide = reverseSMILES(tempPeptide);      
        cout << "revTempPeptide: " << revTempPeptide << endl;
         modNum = 0;

        // glycosylation
        if( postModifications.getElemAtIndex(i) == "GL" )
        {
          postGlycosylation( tempPeptide, j, *postCopy, *peptideLibrary );
        }

        // O-methylation
        if( postModifications.getElemAtIndex(i) == "OME" )
        {
          postOMethylation( tempPeptide, j, *postCopy, *peptideLibrary );
        }

        // N-methylation
        if( postModifications.getElemAtIndex(i) == "NME" )
        {
          postNMethylation( tempPeptide, j, *postCopy, *peptideLibrary );
        }

        // S-methylation
        if( postModifications.getElemAtIndex(i) == "SME" )
        {
          postSMethylation( tempPeptide, j, *postCopy, *peptideLibrary );
        }

        // Hydroxylation
        if( postModifications.getElemAtIndex(i) == "HYD" )
        {
          postHydroxylation( tempPeptide, j, *postCopy, *peptideLibrary );
        }
        
        if( postModifications.getElemAtIndex(i) == "CL" )
        {
          postChlorination( tempPeptide, j, *postCopy, *peptideLibrary );
        }

        if( postModifications.getElemAtIndex(i) == "BR" )
        {
          postBromination( tempPeptide, j, *postCopy, *peptideLibrary );
        }

        if( postModifications.getElemAtIndex(i) == "ID" )
        {
          postIodination( tempPeptide, j, *postCopy, *peptideLibrary );
        }

        if( postModifications.getElemAtIndex(i) == "CY" )
        {
          postCyclization( tempPeptide, j, *postCopy, *peptideLibrary, forw);
          postCyclization( revTempPeptide, j, *postCopy, *peptideLibrary, rev );
        }
        if( postModifications.getElemAtIndex(i) == "ES" )
        {
          postEsterCyclization( tempPeptide, j, *postCopy, *peptideLibrary );
        }
      } // end all peptides

      postCopy =  *&peptideLibrary;
      numPeptides = peptideLibrary->getNumElems();
    } // end all modifications
      emptyLibrary = *peptideLibrary; 
  } //end(if(inputModification) loop
} // end postModification function



void ReactionClass::postGlycosylation (
                               string tempPeptide,
                               int j,
                               QueueClass< UnitClass > &postCopy,
                               QueueClass< UnitClass > &peptideLibrary
                                      )
{
  int modNum;
  int modArraySize;
  int countMods;
  int tempLength;
  int ringNum = 3;
  float mass;
  bool changed = false;
  string formula;

  QueueClass< string > modArray;
  UnitClass peptide, fromPostCopy;

  cout << "GLYCOSYLATION!!!!!!!!!!!!!!!!!!!!!" << endl;

  modNum = 0;
  for( int k = 0; k < tempPeptide.length() - 1; k++ ) // count
  {
    // sites to glycosylate
    if( tempPeptide[k] == 'O' && tempPeptide[k+1] == ')'
        && ( tempPeptide[k-1] == 'C' || tempPeptide[k-1] == '(' ) )
    {
      modNum++;
    }
  }
  modArray = libGen( modNum );  // generate library of sites
                                // to modify
  modArraySize = modArray.getNumElems();

  // glycosylate sites once
  for( int m = 0; m < modArraySize; m++ )
  {
    fromPostCopy = postCopy.getElemAtIndex(j);
    tempPeptide = fromPostCopy.getSmile();
    countMods = 0;

    for( int k = 0; k < tempPeptide.length() - 1; k++ )
    {
      if( tempPeptide[k] == 'O' && tempPeptide[k+1] == ')'
          && ( tempPeptide[k-1] == 'C' || tempPeptide[k-1] == '(' ) )
      {
        countMods++;
      }
      if( tempPeptide[k] == 'O' && tempPeptide[k+1] == ')'
          && ( tempPeptide[k-1] == 'C' ||tempPeptide[k-1] == '(' )
          && modArray.getElemAtIndex(m)[countMods-1] == '1' )
      {
        std::stringstream ss;
        std::string str;
        ss << ringNum;
        ss >> str;
        tempLength = tempPeptide.length();
        tempPeptide.insert( k+1, "C"+ str + "C(O)C(O)C(O)C(C(O))O" +str );
        k += tempPeptide.length() - tempLength;
        changed = true;
      }
    }
    for( int a = 0; a < tempPeptide.length(); a++)
    {
      cout << tempPeptide[a];
    }
    
    cout << endl;
    if( changed )
    {
      massAndFormula( tempPeptide, mass, formula );
      peptide.setUnit( "0", tempPeptide );
      peptide.setMass( mass );
      peptide.setFormula( formula );
      peptideLibrary.insertValue( peptide );
      changed = false;  
    }
    ringNum++;
    if( ringNum == 9)
    {
      ringNum = 4;
    }
  } // end modArray
  // end modification of one peptide of one modArray elemnet
} // end Glycosylation


void ReactionClass::postOMethylation (
                          string tempPeptide,
                          int j,
                          QueueClass< UnitClass > &postCopy,
                          QueueClass< UnitClass > &peptideLibrary
                                     )
{
  int modNum = 0;
  int modArraySize;
  int countMods;
  int tempLength;
  int ringNum = 3;
  float mass;
  string formula;
  bool changed = false;

  QueueClass< string > modArray;
  UnitClass peptide;
  for( int k = 0; k < tempPeptide.length() - 1; k++) // count
  {
    // sites to O-methylate
    if(tempPeptide[k] == 'O' && tempPeptide[k+1] == ')'
        && (tempPeptide[k-1] == '(' || tempPeptide[k-1] == 'C'))
    {
      modNum++;
    }
  }

  modArray = libGen( modNum );  // generate library of sites
                                // to modify
  modArraySize = modArray.getNumElems();
  // O-methylate sites once
  for(int m = 0; m < modArraySize; m++)
  {
    tempPeptide = postCopy.getElemAtIndex(j).getSmile();
    countMods = 0;
    for( int k = 0; k < tempPeptide.length() - 1; k++)
    {
      if(tempPeptide[k] == 'O'
         && tempPeptide[k+1] == ')'
         && (tempPeptide[k-1] == 'C' ||
         tempPeptide[k-1] == '('))
      {
        countMods++;
      }
      if(tempPeptide[k] == 'O' && tempPeptide[k+1] == ')'
         && (tempPeptide[k-1] == 'C' ||tempPeptide[k-1] == '(')
         && modArray.getElemAtIndex(m)[countMods-1] == '1')
      {
        tempLength = tempPeptide.length();
        tempPeptide.insert(k+1,"C");
        k = k + tempPeptide.length() - tempLength;
        changed = true;
      }
    } 
    for( int a = 0; a < tempPeptide.length(); a++)
    {
      cout << tempPeptide[a];
    }
    
    cout << endl;
    if(changed)
    {           
      massAndFormula(tempPeptide, mass, formula);
      peptide.setUnit( "0", tempPeptide);
      peptide.setMass(mass);
      peptide.setFormula(formula);
      peptideLibrary.insertValue(peptide);
      changed = false;
    }
  }
                     
} // end O-methylationClass peptide;



void ReactionClass::postNMethylation (
                          string tempPeptide,
                          int j,
                          QueueClass< UnitClass > &postCopy,
                          QueueClass< UnitClass > &peptideLibrary
                                     )
{
  int modNum = 0;
  int modArraySize;
  int countMods;
  int tempLength;
  int ringNum = 3;
  float mass;
  string formula;
  bool changed = false;

  QueueClass< string > modArray;
  UnitClass peptide;

  for ( int k = 0; k < tempPeptide.length() - 1; k++ ) // count
  {
    // sites to N-methylate
    
      if( tempPeptide[k] == 'N' && ( (tempPeptide[k+4] != '(' && k == 0 && tempPeptide[k+4] != '0') || ( 
      (tempPeptide[k-1] == '(' && tempPeptide[k+4] != '(') 
       || tempPeptide[k+1] == '[' || tempPeptide[k+1] == 'C' || tempPeptide[k+1] == ')'
       || tempPeptide[k-1] == 'C' && tempPeptide[k+4] != '(' 
        && tempPeptide[k+4] != 'C') 
        || (tempPeptide[k+2] == 'C' && tempPeptide[k+4] == ')' && tempPeptide[k-1] != '=' )))
      {
        modNum++;
      }
  }
  modArray = libGen( modNum );  // generate library of sites to modify
  modArraySize = modArray.getNumElems();
  // methylate sites once
  for( int m = 0; m < modArraySize; m++ )
  {
    tempPeptide = postCopy.getElemAtIndex(j).getSmile();
    countMods = 0;
    for( int k = 0; k < tempPeptide.length() - 1; k++ )
    {
     
      if( tempPeptide[k] == 'N' && ( (tempPeptide[k+4] != '(' && k == 0 && tempPeptide[k+4] != '0') || ( 
      (tempPeptide[k-1] == '(' && tempPeptide[k+4] != '(') 
       || tempPeptide[k+1] == '[' || tempPeptide[k+1] == 'C' || tempPeptide[k+1] == ')'
       || tempPeptide[k-1] == 'C' && tempPeptide[k+4] != '(' 
        && tempPeptide[k+4] != 'C') 
        || (tempPeptide[k+2] == 'C' && tempPeptide[k+4] == ')' && tempPeptide[k-1] != '=' )) )
      { 
        countMods++;
      }

      if( tempPeptide[k] == 'N' && ( (tempPeptide[k+4] != '(' && k == 0 && tempPeptide[k+4] != '0') || ( 
      (tempPeptide[k-1] == '(' && tempPeptide[k+4] != '(') 
       || tempPeptide[k+1] == '[' || tempPeptide[k+1] == 'C' || tempPeptide[k+1] == ')'
       || tempPeptide[k-1] == 'C' && tempPeptide[k+4] != '(' 
        && tempPeptide[k+4] != 'C') 
        || (tempPeptide[k+2] == 'C' && tempPeptide[k+4] == ')' && tempPeptide[k-1] != '=')) 
             && modArray.getElemAtIndex(m)[countMods-1] == '1' )
      {
        tempLength = tempPeptide.length();
        tempPeptide.insert( k+1, "(C)" );
        k += tempPeptide.length() - tempLength;
        changed = true;
      }
    }
    for( int a = 0; a < tempPeptide.length(); a++)
    {
      cout << tempPeptide[a];
    }
    
    cout << endl;
    if ( changed )
    {
      massAndFormula( tempPeptide, mass, formula );
      peptide.setUnit( "0", tempPeptide );
      peptide.setMass( mass );
      peptide.setFormula( formula );
      peptideLibrary.insertValue( peptide );
      changed = false;
    }
  }
} // end N-methylation

void ReactionClass::postSMethylation (
                          string tempPeptide,
                          int j,
                          QueueClass< UnitClass > &postCopy,
                          QueueClass< UnitClass > &peptideLibrary
                                     )
{
  int modNum = 0;
  int modArraySize;
  int countMods;
  int tempLength;
  int ringNum = 3;
  float mass;
  string formula;
  bool changed = false;

  QueueClass< string > modArray;
  UnitClass peptide;
  for( int k = 0; k < tempPeptide.length() - 1; k++) // count
  {
    // sites to S-methylate
    if(tempPeptide[k] == 'S' && tempPeptide[k+1] == ')'
        && (tempPeptide[k-1] == '(' || tempPeptide[k-1] == 'C'))
    {
      modNum++;
    }
  }

  modArray = libGen( modNum );  // generate library of sites
                                // to modify
  modArraySize = modArray.getNumElems();
  // S-methylate sites once
  for(int m = 0; m < modArraySize; m++)
  {
    tempPeptide = postCopy.getElemAtIndex(j).getSmile();
    countMods = 0;
    for( int k = 0; k < tempPeptide.length() - 1; k++)
    {
      if(tempPeptide[k] == 'S'
         && tempPeptide[k+1] == ')'
         && (tempPeptide[k-1] == 'C' ||
         tempPeptide[k-1] == '('))
      {
        countMods++;
      }
      if(tempPeptide[k] == 'S' && tempPeptide[k+1] == ')'
         && (tempPeptide[k-1] == 'C' ||tempPeptide[k-1] == '(')
         && modArray.getElemAtIndex(m)[countMods-1] == '1')
      {
        tempLength = tempPeptide.length();
        tempPeptide.insert(k+1,"C");
        k = k + tempPeptide.length() - tempLength;
        cout << "INSERTING METHYL ON SULFUR" << endl;
      }
    }            
    massAndFormula(tempPeptide, mass, formula);
    peptide.setUnit( "0", tempPeptide);
    peptide.setMass(mass);
    peptide.setFormula(formula);
    peptideLibrary.enqueue(peptide);
    cout << peptide.getSmile() << " " << peptide.getMass() << " " << peptide.getFormula() << endl;
  }
                     
} // end S-methylationClass peptide;

void ReactionClass::postHydroxylation (
                          string tempPeptide,
                          int j,
                          QueueClass< UnitClass > &postCopy,
                          QueueClass< UnitClass > &peptideLibrary
                                     )
{
  int modNum = 0;
  int modArraySize;
  int countMods = 0;
  int siteCount;
  int tempLength;
  int countInsert = 0;
  float mass;
  string formula;
  bool changed = false;

  QueueClass< string > modArray;
  UnitClass peptide;

  for ( int k = 0; k < tempPeptide.length() - 1; k++ ) // count
  {
    // sites to hydroxylate
    
      if((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || (tempPeptide[k+1] == '1' && tempPeptide[k+2] == ')' ) ) ) )  
      {
        modNum++;
      }
  }
  modArray = libGen( modNum );  // generate library of sites to modify
  modArraySize = modArray.getNumElems();
  // methylate sites once
  for( int m = 0; m < modArraySize; m++ )
  {
    tempPeptide = postCopy.getElemAtIndex(j).getSmile();
    countMods = 0;
    for( int k = 0; k < tempPeptide.length() - 1; k++ )
    {
          
 
          if((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || (tempPeptide[k+1] == '1' && tempPeptide[k+2] == ')' ) ) ) )
      { 
        countMods++;
      }

      if(((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || (tempPeptide[k+1] == '1' && tempPeptide[k+2] == ')' ) ) )
          )&& modArray.getElemAtIndex(m)[countMods-1] == '1' )
      {
        //cout << "countMods: " << countMods - 1 << "carbon: " << tempPeptide[k] << endl;
        tempLength = tempPeptide.length();
        tempPeptide.insert( k+1, "(O)" );
        k += tempPeptide.length() - tempLength;
        countInsert++;
        changed = true;
        
      }
      
    }
    
    for( int a = 0; a < tempPeptide.length(); a++)
    {
      cout << tempPeptide[a];
    }
    
    cout << endl;
    if ( changed )
    {
      massAndFormula( tempPeptide, mass, formula );
      peptide.setUnit( "0", tempPeptide );
      peptide.setMass( mass );
      peptide.setFormula( formula );
      peptideLibrary.insertValue( peptide );
      changed = false;
    }
  }

} // end Hydroxylation


void ReactionClass::postChlorination (
                          string tempPeptide,
                          int j,
                          QueueClass< UnitClass > &postCopy,
                          QueueClass< UnitClass > &peptideLibrary
                                     )
{
  int modNum = 0;
  int modArraySize;
  int countMods = 0;
  int siteCount;
  int tempLength;
  int countInsert = 0;
  float mass;
  string formula;
  bool changed = false;

  QueueClass< string > modArray;
  UnitClass peptide;

  for ( int k = 0; k < tempPeptide.length() - 1; k++ ) // count
  {
    // sites to chlorinate
    
      if((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || (tempPeptide[k+1] == '1' && tempPeptide[k+2] == ')' ) ) ) )  
      {
        modNum++;
      }
  }
  modArray = libGen( modNum );  // generate library of sites to modify
  modArraySize = modArray.getNumElems();
  // methylate sites once
  for( int m = 0; m < modArraySize; m++ )
  {
   /* for(int z = 0; z < modNum; z++)
    {
      cout << modArray.getElemAtIndex(m)[z];
    }
    cout << endl;
   */
    tempPeptide = postCopy.getElemAtIndex(j).getSmile();
    countMods = 0;
    for( int k = 0; k < tempPeptide.length() - 1; k++ )
    {
          
 
          if((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || (tempPeptide[k+1] == '1' && tempPeptide[k+2] == ')' ) ) ) )
      { 
        countMods++;
      }

      if(((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || (tempPeptide[k+1] == '1' && tempPeptide[k+2] == ')' ) ) )
          )&& modArray.getElemAtIndex(m)[countMods-1] == '1' )
      {
        //cout << "countMods: " << countMods - 1 << "carbon: " << tempPeptide[k] << endl;
        tempLength = tempPeptide.length();
        tempPeptide.insert( k+1, "(Cl)" );
        k += tempPeptide.length() - tempLength;
        countInsert++;
        changed = true;
        
      }
      
    }
    
    for( int a = 0; a < tempPeptide.length(); a++)
    {
      cout << tempPeptide[a];
    }
    
    cout << endl;
    if ( changed )
    {
      massAndFormula( tempPeptide, mass, formula );
      peptide.setUnit( "0", tempPeptide );
      peptide.setMass( mass );
      peptide.setFormula( formula );
      peptideLibrary.insertValue( peptide );
      changed = false;
    }
  }

} // end Chlorination


void ReactionClass::postBromination (
                          string tempPeptide,
                          int j,
                          QueueClass< UnitClass > &postCopy,
                          QueueClass< UnitClass > &peptideLibrary
                                     )
{
  int modNum = 0;
  int modArraySize;
  int countMods = 0;
  int siteCount;
  int tempLength;
  int countInsert = 0;
  float mass;
  string formula;
  bool changed = false;

  QueueClass< string > modArray;
  UnitClass peptide;

  for ( int k = 0; k < tempPeptide.length() - 1; k++ ) // count
  {
    // sites to brominate
    
      if((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || (tempPeptide[k+1] == '1' && tempPeptide[k+2] == ')' ) ) ) )  
      {
        modNum++;
      }
  }
  modArray = libGen( modNum );  // generate library of sites to modify
  modArraySize = modArray.getNumElems();
  // methylate sites once
  for( int m = 0; m < modArraySize; m++ )
  {
   /* for(int z = 0; z < modNum; z++)
    {
      cout << modArray.getElemAtIndex(m)[z];
    }
    cout << endl;
   */
    tempPeptide = postCopy.getElemAtIndex(j).getSmile();
    countMods = 0;
    for( int k = 0; k < tempPeptide.length() - 1; k++ )
    {
          
 
          if((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || (tempPeptide[k+1] == '1' && tempPeptide[k+2] == ')' ) ) ) )
      { 
        countMods++;
      }

      if(((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || (tempPeptide[k+1] == '1' && tempPeptide[k+2] == ')' ) ) )
          )&& modArray.getElemAtIndex(m)[countMods-1] == '1' )
      {
        //cout << "countMods: " << countMods - 1 << "carbon: " << tempPeptide[k] << endl;
        tempLength = tempPeptide.length();
        tempPeptide.insert( k+1, "(Br)" );
        k += tempPeptide.length() - tempLength;
        countInsert++;
        changed = true;
        
      }
      
    }
    
        for( int a = 0; a < tempPeptide.length(); a++)
    {
      cout << tempPeptide[a];
    }
    
    cout << endl;
    if ( changed )
    {
      massAndFormula( tempPeptide, mass, formula );
      peptide.setUnit( "0", tempPeptide );
      peptide.setMass( mass );
      peptide.setFormula( formula );
      peptideLibrary.insertValue( peptide );
      changed = false;
    }
  }

} // end Bromination


void ReactionClass::postIodination (
                          string tempPeptide,
                          int j,
                          QueueClass< UnitClass > &postCopy,
                          QueueClass< UnitClass > &peptideLibrary
                                     )
{
  int modNum = 0;
  int modArraySize;
  int countMods = 0;
  int siteCount;
  int tempLength;
  int countInsert = 0;
  float mass;
  string formula;
  bool changed = false;

  QueueClass< string > modArray;
  UnitClass peptide;

  for ( int k = 0; k < tempPeptide.length() - 1; k++ ) // count
  {
    // sites to Iodinate
    
      if((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || 
          ((tempPeptide[k+1] == '1' || tempPeptide[k+1] == '2') && tempPeptide[k+2] == ')' ) ) ) )  
      {
        modNum++;
      }
  }
  modArray = libGen( modNum );  // generate library of sites to modify
  modArraySize = modArray.getNumElems();
  // methylate sites once
  for( int m = 0; m < modArraySize; m++ )
  {
   /* for(int z = 0; z < modNum; z++)
    {
      cout << modArray.getElemAtIndex(m)[z];
    }
    cout << endl;
   */
    tempPeptide = postCopy.getElemAtIndex(j).getSmile();
    countMods = 0;
    for( int k = 0; k < tempPeptide.length() - 1; k++ )
    {
          
 
          if((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || 
          ((tempPeptide[k+1] == '1' || tempPeptide[k+1] == '2') && tempPeptide[k+2] == ')' ) ) ) )
      { 
        countMods++;
      }

      if(((tempPeptide[k+1] == 'c' && tempPeptide[k+2] == '1' 
          && tempPeptide[k] == 'C') || (tempPeptide[k] == 'c' && 
          tempPeptide[k+1] != '(' && (tempPeptide[k-1] == '1'
          || tempPeptide[k-1] == 'c' || tempPeptide[k+1] == 'c' || 
          ((tempPeptide[k+1] == '1' || tempPeptide[k+1] == '2') && tempPeptide[k+2] == ')' ) ) )
          )&& modArray.getElemAtIndex(m)[countMods-1] == '1' )
      {
        tempLength = tempPeptide.length();
        tempPeptide.insert( k+1, "(I)" );
        k += tempPeptide.length() - tempLength;
        countInsert++;
        changed = true;
        
      }
      
    }
    for( int a = 0; a < tempPeptide.length(); a++)
    {
      cout << tempPeptide[a];
    }
    
    cout << endl;
    if ( changed )
    {
      massAndFormula( tempPeptide, mass, formula );
      peptide.setUnit( "0", tempPeptide );
      peptide.setMass( mass );
      peptide.setFormula( formula );
      peptideLibrary.insertValue( peptide );
      changed = false;
    }
  }

} // end Iodination

void ReactionClass::postCyclization (
                          string tempPeptide,
                          int j,
                          QueueClass< UnitClass > &postCopy,
                          QueueClass< UnitClass > &peptideLibrary,
                          bool forw)
{
  int modNum = 0;
  int modArraySize;
  int countMods = 0;
  int siteCount;
  int tempLength;
  int countInsert = 0;
  int minCy;
  int maxCy;
  int limit;
  int change;
  int forwOrRev = 0;
  string Ocy = "8";
  string Scy = "9";
  float mass;
  string formula;
  bool changed = false;
  int kStore;
  bool nuclCyc = false;
  QueueClass< string > modArray;
  UnitClass peptide;
  string halo;
  const string tempPeptideStore = tempPeptide;
  
  if(forw)
  {
    limit = 0; 
    change = -1;
    minCy = 11;
    maxCy = 16;
  }
  else
  {
    limit = tempPeptide.length();
    change = 1;
    minCy = -12;
    maxCy = -7;
    //cout << "REVERSE..." << endl;
  }
 
  cout << "tempPeptide: " << tempPeptide << endl;

  for ( int k = 0; k < tempPeptide.length() - 1; k++ ) // count
  {
    // sites to cyclize
    
      if( (tempPeptide[k] == 'O' || tempPeptide[k] == 'S' )  
          && tempPeptide[k+1] == ')' && tempPeptide[k-1] != '=')  
      {
        modNum++;
      }
  }
  modArray = libGen( modNum );  // generate library of sites to modify
  modArraySize = modArray.getNumElems();
  // count sites
  for( int m = 0; m < modArraySize; m++ )
  {
    //tempPeptide = postCopy.getElemAtIndex(j).getSmile();
    tempPeptide = tempPeptideStore;
    countMods = 0;
    for( int k = 0; k < tempPeptide.length() - 1; k++ )
    {
      if((tempPeptide[k] == 'O' || tempPeptide[k] == 'S' ) 
         && tempPeptide[k+1] == ')' && tempPeptide[k-1] != '=')  
      { 
        countMods++;
      }
 
      if(((tempPeptide[k] == 'O' || tempPeptide[k] == 'S'  )
          && tempPeptide[k+1] == ')' && tempPeptide[k-1] != '=')
          && modArray.getElemAtIndex(m)[countMods-1] == '1' )
      { 
        
        tempLength = tempPeptide.length();  
        kStore = k; // use as new variable to go to C(=O)
        while(tempPeptide[kStore] != '=' && kStore != limit) // variable based 
                                                        // on forw or rev
        {
          kStore = kStore + change; // variable based on forw or rev
          //cout << "k-kStore: " << k - kStore << endl;
          if(tempPeptide[kStore] == '=' 
             && tempPeptide[kStore+1] == 'O' 
             && (k-kStore < maxCy) && (k-kStore > minCy)) // certain distance
          {                                // over which to heterocyclize
            nuclCyc = true;
          }

        }
          if(tempPeptide[k] == 'O')
          {
            halo = Ocy; // constant for O heterocyclization
          }
          else if(tempPeptide[k] == 'S')
          { 
            halo = Scy; // constant for S heterocyclization
          }

          if(nuclCyc)
          {
            tempPeptide.insert( k+1, halo); 
            if(forw) // variable based on forw or rev
            {
              tempPeptide.insert( kStore - 1, halo);
              tempPeptide.erase(kStore, 4);
              if(tempPeptide[kStore + 1] != '0' && tempPeptide[kStore + 1] != '(')
              {  
                tempPeptide.insert(kStore, "="); // insert double bond
              }
            }
            else
            {
              tempPeptide.insert(kStore, halo);
              tempPeptide.erase(kStore + 1, 4);
              // cout << "tempPeptide[kStore - 4] = " << tempPeptide[kStore - 4] << endl;
              if(tempPeptide[kStore - 4] != '0' && tempPeptide[kStore - 4] != '(')
              { 
                tempPeptide.insert(kStore - 1, "="); // insert double bond
              }
            }

            changed = true;
            nuclCyc = false;
          }
          k += tempPeptide.length() - tempLength;
      }
      
    }
    
    for( int a = 0; a < tempPeptide.length(); a++)
    {
      cout << tempPeptide[a];
    }
    
    cout << endl;
    if ( changed )
    {
      massAndFormula( tempPeptide, mass, formula );
      peptide.setUnit( "0", tempPeptide );
      peptide.setMass( mass );
      peptide.setFormula( formula );
      peptideLibrary.insertValue( peptide );
      changed = false;
    }
  }

} // end Cyclization

void ReactionClass::postEsterCyclization (
                          string tempPeptide,
                          int j,
                          QueueClass< UnitClass > &postCopy,
                          QueueClass< UnitClass > &peptideLibrary
                                     )
{
  int lastInsert = 0;
  string esterCycNum = "0";
  float mass;
  string formula;
  int strainLength = 50;
  QueueClass< string > modArray;
  UnitClass peptide;
  string tempPeptideStore = tempPeptide;

 // cyclize with nucleophilic sites
  for ( int k = lastInsert; k < tempPeptide.length() - 1; k++ ) // count
  {  
    
    if( (tempPeptide[k] == 'O' || tempPeptide[k] == 'S' )  
          && tempPeptide[k+1] == ')' && tempPeptide[k-1] != '='
          && tempPeptide[1] != '0' 
          && tempPeptide.length() - k > strainLength)  
    {
      tempPeptide.insert(k+1, esterCycNum);
      tempPeptide.insert(tempPeptide.length()-5, esterCycNum);
      tempPeptide.erase(tempPeptide.length()-1, 1);

      massAndFormula( tempPeptide, mass, formula );
      peptide.setUnit( "0", tempPeptide );
      peptide.setMass( mass );
      peptide.setFormula( formula );
      peptideLibrary.insertValue( peptide );
      
      lastInsert = k+2; 
      tempPeptide = postCopy.getElemAtIndex(j).getSmile();  
    }
  }
  for( int a = 0; a < tempPeptide.length(); a++)
  {
    cout << tempPeptide[a];
  }    
  cout << endl;

} // end EsterCyclization

