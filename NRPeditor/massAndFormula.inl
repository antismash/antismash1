#include <iostream>
#include <string>
#include <sstream>
using namespace std;

// convert integer to string
std::string IntToString ( int number )
{
  std::ostringstream oss;

  // Works just like cout
  oss<< number;

  // Return the underlying string
  return oss.str();
}

// calculate mass and write out chemical formula of the input chemical SMILES
void massAndFormula( const string smile, float &mass, string &formula )
{
  int smileLength;
  // set all counts to zero
  int CCount = 0;
  int NCount = 0;
  int OCount = 0;
  int SCount = 0;
  int ClCount = 0;
  int BrCount = 0;
  int ICount = 0;
  int HCount = 0;
  int doubleCount = 0;
  int tripleCount = 0;
  int aromaticCount = 0;
  int cyclCount = 0;
  
  smileLength = smile.length();
  mass = 0.0000;       // reset mass to 0

  for(int i = 0; i <= smileLength; i++) // loop from beginning to end of smile
  {                              // and calculate atoms and unsaturation units
    if(smile[i] == 'C' && smile[i+1] != 'l')
    {
      CCount++;
    }
    if(smile[i] == 'N')
    {
      NCount++;
    }
    if(smile[i] == 'O')
    {
      OCount++;
    }
    if(smile[i] == 'S')
    {
      SCount++;
    }
    if(smile[i] == 'C' && smile[i+1] == 'l')
    {
      NCount++;
    }
    if(smile[i] == 'B' && smile[i+1] == 'r')
    {
      BrCount++;
    }
    if(smile[i] == 'I')
    {
      ICount++;
    }
    if(smile[i] == '=')
    {
      doubleCount++;
    }
    if(smile[i] == '#')
    {
      tripleCount++;
    } 
    if(smile[i] == 'c')
    {
      CCount++;
      aromaticCount++; // number of aromatic units
    }
    if(smile[i] == 'n')
    {
      NCount++;
      aromaticCount++; 
    }
    // look for the number of number set (ring)
    if ( static_cast< unsigned int >( smile[i] ) >= 48 && 
         static_cast< unsigned int >( smile[i] ) <= 57 )
    {
      cyclCount++;
    }   
  }

  // formula for calculating number of hydrogens
  HCount = (( CCount + NCount ) * 2) + 2 - NCount - ClCount - BrCount - ICount 
           - doubleCount * 2 - tripleCount * 4 - aromaticCount - cyclCount;

  // formula for calculating mass
  mass = CCount * C + NCount * N + OCount * O + HCount * H + ClCount * Cl 
         + BrCount * Br + ICount * I;

  // write out chemical formula
  formula.erase();
  std::string CNum = IntToString( CCount );

  formula = "C" + CNum + "H" + IntToString( HCount );
  if ( NCount > 0 )
  {
    formula += ( "N" + IntToString( NCount ) );
  }
  if  ( OCount > 0 )
  {
    formula += ( "O" + IntToString( OCount ) );
  }
  if ( SCount > 0 )
  {
    formula += ( "S" + IntToString( SCount ) );
  }
  if ( ClCount > 0 )
  {
    formula += ( "Cl" + IntToString( ClCount ) );
  }
  if ( BrCount > 0 )
  {
    formula += ( "Br" + IntToString( BrCount ) );
  }
  if ( ICount > 0 )
  {
    formula += ( "I" + IntToString( ICount ) );
  }

}



    
