#ifndef __SPAWNL_H__
#define __SPAWNL_H__

int spawnl(int mode, const char *path, ...);

#endif
