#include "UnitClass.h"
#include <iostream>
#include <string>

using namespace std;

#include "constants.h"

//---------------------------------------------------------------------//

// operator overloadings for mass comparison
// operator < is reserved to use for insertValue() function of QueueClass
bool UnitClass::operator< ( const UnitClass &rhs ) const
{
  return( this->getMass() < rhs.getMass() );
}
bool UnitClass::operator<= ( const float rhs ) const
{
  return( this->getMass() <= rhs );
}

bool UnitClass::operator> ( const float rhs ) const
{
  return( this->getMass() > rhs );
}

bool UnitClass::operator>= ( const float rhs ) const
{
  return( this->getMass() >= rhs );
}

//----------------------------------------------------------------------//

void UnitClass::setUnit( const string inUnit, const string inSmile )
{
  unit = inUnit;
  smileString = inSmile;
}

void UnitClass::setSmileString( const string inSmile )
{
  smileString = inSmile;
}

void UnitClass::setMass( const float inMass )
{
  mass = inMass;
}

void UnitClass::setStartSite( const int inSite )
{
  startSite = inSite;
}

void UnitClass::setEndSite( const int inSite )
{
  endSite = inSite;
}

void UnitClass::setFormula( const string inFormula )
{
  formula = inFormula;
}

void UnitClass::setSequence( const string inSequence )
{
  sequence = inSequence;
}

string UnitClass::getUnit() const
{
  return unit;
}

string UnitClass::getSmile() const
{
  return smileString;
}

float UnitClass::getMass() const
{
  return mass;
}

int UnitClass::getStartSite() const
{
  return startSite;
}

int UnitClass::getEndSite() const
{
  return endSite;
}

string UnitClass::getFormula( ) const
{
  return formula;
}

string UnitClass::getSequence( ) const
{
  return sequence;
}


