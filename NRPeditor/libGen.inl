#include <iostream>
#include <cmath>
#include <string>
#include "QueueClass.h"
#include "ListNodeClass.h"

QueueClass< string > libGen( int num)
{
  int i;
  int j;
  int val;
  int pos = 0;
  string myAry = "000000000000000000";
  string temp;
  QueueClass < string > modArray;
 
  for (i = 0; i < (int)pow((double)2, (double)num); i++)
  {
    //cout << "i is: " << i;
    for (j = 0; j < num; j++)
    {
      val = (int)pow((double)2, (double)j);
      if (i & val)
      {
        myAry[j] = '1';
      }
      else
      {
        myAry[j] = '0';
      }
    }
    modArray.enqueue(myAry);
  }  

  return modArray;
}
