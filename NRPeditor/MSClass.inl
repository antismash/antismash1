#include <iostream>
#include <string>
using namespace std;

#include "QueueClass.h"
#include "UnitClass.h"
#include "constants.h"

void MSClass::setValues( const float inMin, const float inMax )
{
  lowerLimit = inMin;
  upperLimit = inMax;
  cout << " min " << lowerLimit << " max " << upperLimit << endl;
}

float MSClass::getUpperLimit( ) const
{
  return ( upperLimit );
}

float MSClass::getLowerLimit( ) const
{
  return ( lowerLimit );
}

bool MSClass::setRange(float minMass, float maxMass)
{
  float inputMax;
  float inputMin;
  bool validInput = false;

  // error checking; only integer within limit is accepted
  cout << "Enter MS range from minimum to maximum: ";

  //cin >> inputMin >> inputMax;
  // max daltons of molecule
  inputMin = minMass;
  inputMax = maxMass;
  // check if input is of right type
  if ( cin.fail() )
  {
    cin.clear();
    cin.ignore(200, '\n');
    cout << "Invalid input type!" << endl;
  }
  // check if input residue number is within limit
  else if ( inputMin > MS_MIN && inputMin < inputMax && inputMax <= MS_MAX )
  {
    validInput = true;
    setValues( inputMin, inputMax );
  }
  else
  {
    cout << "Entered masses"
         << " are out of current supporting limit" << endl;
    validInput = false;
  }
  return ( validInput );
}

void MSClass::writeToFile( const string &fname, 
                           QueueClass< UnitClass > &list,
                           QueueClass< UnitClass > &output,
                           string predictedSubstrates) const
{
  bool success = true;
  int numElems;
  UnitClass node;
  ofstream outFile;
  string molecules;
  const int minSmilesLength = 12;

  outFile.open( fname.c_str() );
  // check if can write out MS filter file
  if ( outFile.fail() )
  {
    cout << "Error: Cannot open output file!" << endl;
    success = false;
  }
  else
  {
    success = true;
    // header codes magic number to signify PPM file type
    
    outFile << "> " << predictedSubstrates << "\n";
    numElems = list.getNumElems();
    for ( int i = 0; i < numElems; i++ )
    {
      node = list.getElemAtIndex( i );
      if ( node >= getLowerLimit() && node <= getUpperLimit() )
      {
        if(node.getSmile().length() > minSmilesLength) // only large smiles allowed
        {
          outFile << (node.getSmile());
          if(i < numElems - 1)
          {
            outFile << ".";
          }
        }
        output.insertValue( node );
      
      if ( i+1 < numElems )
      {
        if ( list.getElemAtIndex( i+1 ) > getUpperLimit() )
        {
          i = numElems-1;
        }
        else 
        {
        }
      }
     }
    }
  }


  outFile.close();
}

