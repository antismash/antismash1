#include <iostream>
#include <string>
#include <sstream>
#include "deleteRepeat.h"
#include "QueueClass.h"
#include "UnitClass.h"
#include "ReactionClass.h"

using namespace std;

void deleteRepeat( QueueClass< UnitClass > &peptideLibrary)
{

  QueueClass< UnitClass > *nonRepeatList;
  nonRepeatList = new QueueClass< UnitClass >;
  UnitClass peptide;
  int listSize;
  int copyListSize;
  bool alreadyAdded = 0; 
  int nR = 0;
  int R = 0;
  copyListSize = 0;
  listSize = peptideLibrary.getNumElems();
  string reversed;

  for(int i = 0; i < listSize; i++)
  {
    for(int j = 0; j < copyListSize; j++)
    {
      if(nonRepeatList->getNumElems() > 0)
      {
        reversed = reverseSMILES(nonRepeatList->getElemAtIndex(j).getSmile());
      }
      if(string(peptideLibrary.getElemAtIndex(i).getSmile()) 
         == string(nonRepeatList->getElemAtIndex(j).getSmile()))
      { 
        alreadyAdded = 1;
      }
        
      if(string(peptideLibrary.getElemAtIndex(i).getSmile())
          == string(reversed))
      {
        alreadyAdded = 1;
      } 
   
    }
    
    if(!alreadyAdded)
    {
        peptide = peptideLibrary.getElemAtIndex(i);
        nonRepeatList->insertValue(peptide); 
        nR++;
        //cout << nR <<". " << "insert" << endl;  
    }
    
    copyListSize = nonRepeatList->getNumElems();
    alreadyAdded = 0;
    
  }

  peptideLibrary.clear();
  
  listSize = nonRepeatList->getNumElems();
  
  for(int i = 0; i < listSize; i++)
  {
    // cout << "composing nonrepeat list" << endl;
    peptideLibrary.insertValue(
    nonRepeatList->getElemAtIndex(i));
  }
  
  nonRepeatList->clear();
}

string reverseSMILES( const string &forSMILES)
{
  string revSMILES = "";
  int bracket = 0;
  int iStore;  
 
  // go through forward SMILES backwards 
  for(int i = forSMILES.length(); i >= 0; i--)
  {
    iStore = i;
    if(forSMILES[i] == ')' // if there is a close parentheses
       || forSMILES[i] == ']')
    { 
      bracket++;
    }

    if(forSMILES[i] == '(' // if there is a close parentheses
       || forSMILES[i] == '[')
    { 
      bracket--;
    }           
    while(i != 0 && (bracket != 0 || (forSMILES[i-1] == ']' && forSMILES[i] == '(')
          || (forSMILES[i] == '(' && (forSMILES[i+1] == '='
          || forSMILES[i+1] == 'C' || forSMILES[i+1] == 'O'
          || forSMILES[i+1] == 'S' || forSMILES[i+1] == '[' 
          || forSMILES[i+1] == 'N'))
          || (static_cast< unsigned int >( forSMILES[i]) >= 48 && 
		static_cast< unsigned int >( forSMILES[i]) <= 57 )
            || (static_cast< unsigned int >( forSMILES[i]) >=97 &&
                static_cast< unsigned int >( forSMILES[i]) <= 122)))
    { 
      i--;
      if(forSMILES[i] == ')' // if there is a close parentheses
         || forSMILES[i] == ']')
      { 
        bracket++;
      }

      if(forSMILES[i] == '(' // if there is a close parentheses
         || forSMILES[i] == '[')
      { 
        bracket--;
      }       
    }

    revSMILES.insert(revSMILES.length(), forSMILES.substr(i, iStore-i+1));  
  }
  return revSMILES;
}
