#ifndef _UNITCLASS2_H_
#define _UNITCLASS2_H_

#include "constants.h" 

class UnitClass2
{
  private:
  string unit;              // amino acid, or other substrate
  string smileString; 
  string formula;
  float mass; 
  int startSite;
  int endSite;
  int nucStartSite;
  int nucEndSite;

  public:

    bool operator< ( const UnitClass2 & rhs ) const;
    bool operator<= ( const float rhs ) const;
    bool operator> ( const float rhs ) const;
    bool operator>= ( const float rhs ) const;

    void setUnit( const string inUnit, const string inSmile );

    void setSmileString( const string inSmile );

    void setMass( const float inMass );
 
    void setStartSite( const int inSite);

    void setEndSite( const int inSite);

    void setNucStartSite( const int inSite);

    void setNucEndSite( const int inSite);

    void setFormula( const string inFormula );

    string getUnit( ) const;

    string getSmile( ) const;

    float getMass( ) const;

    int getStartSite( ) const;

    int getEndSite( ) const;

    int getNucStartSite( ) const;

    int getNucEndSite( ) const;

    string getFormula( ) const;

};

#include "UnitClass2.inl"
#endif
