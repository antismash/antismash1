#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include "constants.h" 
#include "QueueClass.h"
#include "UnitClass.h"

bool readDatabase( const string &fname,
                   const string inResidue,
                   string &smileString )
{
  bool success;
  bool reachEnd = false;
  bool found = false;
  string residueName;
  string dumped;           // this string will not be used.
  ifstream inFile;

  inFile.open( fname.c_str() );
  // check file existence
  if ( inFile.fail() )
  {
    cout << "Error: Input file " << fname.c_str() << " fails to open!" << endl;
    success = false;
  }
  else
  {
    inFile >> dumped;
    if ( inFile.eof() )
    {
      cout << "Error: End-of-file before reading!" << endl;
      success = false;
    }
    else
    {
      success = true;
    }

    // if database file found, search for amino acid residue
    if ( success )
    {
      // search for the residue 
      while ( !found )
      {
        inFile >> residueName;
        if ( residueName == "END" )
        {
          found = true;
          reachEnd = true;
        }
        else if ( residueName == inResidue )
        {
          inFile >> smileString;
          found = true;
        }
        else
        {
          inFile >> dumped;
          found = false;
        }
      }
    }
  }
  if ( reachEnd )
  {
    success = false;
  }

  inFile.close();
  return( success );
}


bool readMassFile( const string &fname,
                   float mass[] )
{
  bool success;
  bool found = false;
  float peak;
  string dumped;           // this string will not be used.
  ifstream inFile;

  inFile.open( fname.c_str() );
  // check file existence
  if ( inFile.fail() )
  {
    cout << "Error: Input file fails to open!" << endl;
    success = false;
  }
  else
  {
    inFile >> dumped;
    if ( inFile.eof() )
    {
      cout << "Error: End-of-file before reading!" << endl;
      success = false;
    }
    else if ( inFile.fail() )
    {
      inFile.clear();
      inFile.ignore(200, '\n');
    }
    else
    {
      success = true;
    }

    // if database file found, search for amino acid residue
    if ( success )
    {
      int i = 0;
      // search for the residue
      while ( !inFile.eof() )
      {
        inFile >> peak;
        mass[i] = peak;
        i++;
      }
    }
  }
  inFile.close();
  return( success );
}



void writeToFile( const string &fname,
                  QueueClass< UnitClass > &peptideLibrary )
{
  int numElems;
  bool success = true;
  ofstream outFile;
  
  UnitClass node;

  outFile.open( fname.c_str() );
  // check if can write out image file
  if ( outFile.fail() )
  {
    cout << "Error: Cannot open output file!" << endl;
  }
  else 
  {
    numElems = peptideLibrary.getNumElems();
    float maxMass = peptideLibrary.getElemAtIndex( numElems-1 ).getMass();
    // cout << "maxMass " << maxMass << endl;
    cout << endl;
    for ( int i = 0; i < maxMass; i+=100 )
    {
      // outFile << endl << "Range: " << i << " ~ " << i+100 << endl << endl;
      for ( int j = 0; j < numElems; j++ )
      {
        node = peptideLibrary.getElemAtIndex( j );
        if ( node >= i && node <= i+100 )
        {
          outFile << "." << node.getSmile();
          cout << " " << node.getSmile() << " " << node.getMass() << endl;


          if ( j+1 < numElems )
          {
            if ( peptideLibrary.getElemAtIndex( j+1 ) > (i + 100) )
            {
              j = numElems-1;
              outFile << endl;
            }
            else
            {
              //if ( j > 0 )
              //{outFile << "."; }
            }
          }
        }
      }
    }
  }
  outFile.close();
}

