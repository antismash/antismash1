#include "UnitClass2.h"
#include <iostream>
#include <string>

using namespace std;

#include "constants.h"

//---------------------------------------------------------------------//

// operator overloadings for mass comparison
// operator < is reserved to use for insertValue() function of QueueClass
bool UnitClass2::operator< ( const UnitClass2 &rhs ) const
{
  return( this->getNucStartSite() < rhs.getNucStartSite() );
}
bool UnitClass2::operator<= ( const float rhs ) const
{
  return( this->getNucStartSite() <= rhs );
}

bool UnitClass2::operator> ( const float rhs ) const
{
  return( this->getNucStartSite() > rhs );
}

bool UnitClass2::operator>= ( const float rhs ) const
{
  return( this->getNucStartSite() >= rhs );
}

//----------------------------------------------------------------------//

void UnitClass2::setUnit( const string inUnit, const string inSmile )
{
  unit = inUnit;
  smileString = inSmile;
}

void UnitClass2::setSmileString( const string inSmile )
{
  smileString = inSmile;
}

void UnitClass2::setMass( const float inMass )
{
  mass = inMass;
}

void UnitClass2::setStartSite( const int inSite )
{
  startSite = inSite;
}

void UnitClass2::setEndSite( const int inSite )
{
  endSite = inSite;
}

void UnitClass2::setNucStartSite( const int inSite )
{
  nucStartSite = inSite;
}

void UnitClass2::setNucEndSite( const int inSite )
{
  nucEndSite = inSite;
}

void UnitClass2::setFormula( const string inFormula )
{
  formula = inFormula;
}

string UnitClass2::getUnit() const
{
  return unit;
}

string UnitClass2::getSmile() const
{
  return smileString;
}

float UnitClass2::getMass() const
{
  return mass;
}

int UnitClass2::getStartSite() const
{
  return startSite;
}

int UnitClass2::getEndSite() const
{
  return endSite;
}

int UnitClass2::getNucStartSite() const
{
  return nucStartSite;
}

int UnitClass2::getNucEndSite() const
{
  return nucEndSite;
}

string UnitClass2::getFormula( ) const
{
  return formula;
}


