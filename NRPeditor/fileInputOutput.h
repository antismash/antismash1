#ifndef _FILEINPUTOUTPUT_H_
#define _FILEINPUTOUTPUT_H_

bool readDatabase( const string &fname,
                   const string inResidue,
                   string &smileString );

bool readMassFile( const string &fname,
                   float mass[] );

void writeToFile( const string &fname,
                  QueueClass< UnitClass > &peptideLibrary );


#include "fileInputOutput.inl"

#endif
