// timeEstimate takes as input a genome sequence
// and outputs the estimated time in minutes
// it will take for clusterFinder to run it
// based on recent runs- currently this is
// three million basepairs per minute, the
// time needed is stored in the file runTime

#include <iostream>
#include <string>
#include <fstream>

using namespace std;
#include "spawnl.h"
int main( int argc, char* argv[])
{
  
  string nucleotideSeq;
  string inputFilename;
  string character;
  ifstream In;
  ofstream runTime;
  float charCount = 0;
  float charTimeRatio = 3000000; // 3 million basepairs per minute (last estimate)
  float charTime;

  if(argc > 1)
  {
    inputFilename = argv[1];
  }
  else
  {
    cout << "No file specified-exiting..." << endl;
    return 0;
  }
  In.open(inputFilename.c_str());
  if(In.fail())
  {
    cout << "Invalid file." << endl;
    return 0;
  }
  else
  {
    In >> character;
    while(!In.fail())
    {
      In >> character;
      charCount = charCount + character.length();
    }

    charTime = charCount / charTimeRatio;
    runTime.open("runTime");
    runTime << charTime << " minutes";
    runTime.close();
  }
   
  cout << charTime << endl;


  /*
  if(argc == 2)
  {
    spawnl(1, "./clusterFinder.exe", "clusterFinder.exe", argv[1],
           NULL);
  }  
  if(argc == 3)
  {
    spawnl(1, "./clusterFinder.exe", "clusterFinder.exe", argv[1], 
           argv[2], NULL);
  }
  if(argc == 4)
  {
    spawnl(1, "./clusterFinder.exe", "clusterFinder.exe", argv[1],
           argv[2], argv[3], NULL);
  }
  if(argc == 5)
  {
    spawnl(1, "./clusterFinder.exe", "clusterFinder.exe", argv[1],
           argv[2], argv[3], argv[4], NULL);
  }
  */
  

  return 0;
}
