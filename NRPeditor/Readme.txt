-----***Program name: NP.searcher v1.0***-------

Authors: Huan Tang Li, Peter MU Ung

Last Date Checked: 4.4.09

Description: NP.searcher takes as input, DNA or amino acid sequences, and outputs the predicted NRPS or PKS natural product small molecules from a stored database of NRPS and PKS signature sequences.

Open-source License: GNU General Public License (GPL)

--->Key Files:
-main.cpp- calls every function and initiates the user input

-clusterFinder.cpp- scans a genome and finds individual clusters, allowing detection of individual small molecules

-ReactionClass.inl-contains the functions for performing all the reactions on modifying the NRPS or PKS small molecule

-getUserInput.inl- translates the genetic information into amino acid sequence and determines the substrates specificity of each domain; builds the SMILES string of each substrate 

--->Non-class Support files: 
-deleteRepeat.inl- deletes repeat SMILES resulting from the modifications implemented from ReactionClass.inl

-fileInputOutput.inl- writes the SMILES string to output files

-libGen.inl- calculates combinatorial possibilities resulting from the number of sites of modification on a certain small molecule

-massAndFormula.inl- determines the mass and formula of a certain small molecule

-MSClass.inl- filters the predicted molecules by mass


--->Information files:
-aaSMILES.txt- contains all the amino acid and ketide in SMILES format

-mods.fasta- lists protein sequences of all modification and auxiliary domains

-NRPSsignatures.fasta- lists signatures of all NRPS domains

-PKSsignatures.fasta- lists signatures of all PKS domains


Main Program sequence:

clusterFinder.cpp-->main.cpp-->getUserInput.inl-->ReactionClass.inl, libGen.inl
-->deleteRepeat, massAndFormula.inl, MSClass.inl, fileInputOutput.inl



