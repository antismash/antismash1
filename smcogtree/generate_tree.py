import os
import sys

#Write FASTA file
def writefasta(names,seqs,file):
  e = 0
  f = len(names) - 1
  out_file = open(file,"w")
  while e <= f:
    out_file.write(">")
    out_file.write(names[e])
    out_file.write("\n")
    out_file.write(seqs[e])
    out_file.write("\n")
    e += 1
  out_file.close()

def sortdictkeysbyvalues(dict):
    items = [(value, key) for key, value in dict.items()]
    items.sort()
    items.reverse()
    return [[key,value] for value, key in items]








smcog = "smcog1000"
gene = "YP_003485938"
genomenamefolder = "../Sclav_chromosome/"

print "Calculating and drawing phylogenetic trees of cluster genes with smCOG members"
#create input.fasta file with single query sequence to be used as input for MSA

#Align to multiple sequence alignment, output as fasta file
fastafile = "input.fasta"
musclecommand = "muscle -quiet -profile -in1 " +smcog + "_muscle.fasta -in2 input.fasta -out muscle.fasta"
os.system(musclecommand)
#Trim alignment 
#edit muscle fasta file: remove all positions before the first and after the last position shared by >33% of all sequences
file = open("muscle.fasta","r")
text = file.read()
lines = text.split("\n")
##Combine all sequence lines into single lines
lines2 = []
seq = ""
nrlines = len(lines)
a = 0
lines = lines[:-1]
for i in lines:
  if a == (nrlines - 2):
    seq = seq + i
    lines2.append(seq)
  if i[0] == ">":
    lines2.append(seq)
    seq = ""
    lines2.append(i)
  else:
    seq = seq + i
  a += 1
lines = lines2[1:]
#Retrieve names and seqs from muscle fasta lines
seqs = []
names = []
for i in lines:
  if len(i) > 0 and i[0] == ">":
    name = i[1:]
    names.append(name)
  else:
    seq = i
    seqs.append(seq)
#Find first and last amino acids shared conserved >33%
#Create list system to store conservation of residues
conservationlist = []
lenseqs = len(seqs[0])
nrseqs = len(seqs)
for i in range(lenseqs):
  conservationlist.append({"A":0,"B":0,"C":0,"D":0,"E":0,"F":0,"G":0,"H":0,"I":0,"J":0,"K":0,"L":0,"M":0,"N":0,"P":0,"Q":0,"R":0,"S":0,"T":0,"U":0,"V":0,"W":0,"X":0,"Y":0,"Z":0,"-":0})
a = 0
for i in seqs:
  aa = list(i)
  for i in aa:
    conservationlist[a][i] += 1
    a += 1
  a = 0
firstsharedaa = 0
lastsharedaa = lenseqs
#Find first amino acid shared
first = "yes"
nr = 0
for i in conservationlist:
  aalist = sortdictkeysbyvalues(i)
  for aa in aalist:
    if aa[0] != "-" and aa[1] > (nrseqs / 3) and first == "yes":
      firstsharedaa = nr
      first = "no"
  nr += 1
#Find last amino acid shared
conservationlist.reverse()
first = "yes"
nr = 0
for i in conservationlist:
  aalist = sortdictkeysbyvalues(i)
  for aa in aalist:
    if aa[0] != "-" and aa[1] > (nrseqs / 3) and first == "yes":
      lastsharedaa = lenseqs - nr
      first = "no"
  nr += 1
#Shorten sequences to detected conserved regions
seqs2 = []
for i in seqs:
  seq = i[firstsharedaa:lastsharedaa]
  seqs2.append(seq)
seqs = seqs2
seedfastaname = "trimmed_alignment.fasta"
writefasta(names,seqs,seedfastaname)
#Draw phylogenetic tree with fasttree 2.1.1
nwkfile = "tree.nwk"
fasttreecommand = "fasttree -quiet trimmed_alignment.fasta > " + nwkfile
os.system(fasttreecommand)
#Convert tree to XTG and draw PNG image using TreeGraph
convertcommand = "java -jar TreeGraph.jar -convert tree.nwk -xtg tree.xtg"
os.system(convertcommand)
imagecommand = "java -jar TreeGraph.jar -image tree.xtg " + gene + ".png"
os.system(imagecommand)
os.system('copy ' + gene + '.png "' + genomenamefolder + '"')
os.system("del " + gene + ".png")
os.system("del tree.xtg")
os.system("del trimmed_alignment.fasta")
